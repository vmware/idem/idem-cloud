from unittest import mock

import pytest


@pytest.fixture(scope="module")
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")

    with mock.patch(
        "sys.argv",
        ["idem", "state"],
    ):
        hub.pop.config.load(["idem"], "idem", parse_cli=True)

    yield hub
